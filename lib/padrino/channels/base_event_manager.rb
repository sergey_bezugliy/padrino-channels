# frozen_string_literal: true

module Padrino
  module Channels
    ##
    # Base Event hadler
    #
    class BaseEventManager
      require 'oj'

      attr_accessor :pub_sub, :connections

      ##
      # Creates a new WebSocket manager for a specific connection with a user on a channel.
      #
      # It will listen for specific events and run their blocks whenever a WebSocket call on
      # that channel comes through.
      #
      # == Params
      # * `channel`: The name of the channel
      # * `user`: Unique string to ID the user. See the `set_websocket_user` helper.
      # * `websocket`: The WebSocket promise/reactor/etc - whatever keeps it alive for this user.
      # * `event_context`: The Padrino application controller's context so that it can access the
      #   helpers, mailers, settings, etc. Making it a first-class citizen as a regular HTTP
      #   action is. TODO Review it though, it's probably wrong.
      # * `&block`: A block with supported events to manbase-event-managerage.
      #
      def initialize(channel, user, websocket, event_context, &block)
        @channel = channel
        @websocket = websocket
        @user = user
        @event_context = event_context
        @connections ||= {}
        @events = {}
        @connections[@channel] ||= {}
        @connections[@channel][@user] = @websocket

        instance_eval(&block) if block
      end

      ##
      # DSL for adding events from the events block
      #
      def event(name, &block)
        @events[name.to_sym] = block if block
      end
      alias on event

      ##
      # Message receiver
      # base-event-manager
      def on_message(data, _websocket)
        # TODO: Detect external websocket hijack and drop it

        # Parse the message
        message = ::Oj.load data

        # Check if we have well formed message, i.e., it includes at least an event name.
        event = message.delete 'event'
        handle_error(message_format) if event.nil?
        event = event.to_sym

        # Check if it's a valid event
        handle_error(unsupported_event) unless @events.include?(event)

        # Call it
        # TODO Make the params (message) available through the params variable as we do
        # in normal actions.
        logger.debug "Calling event: #{event} as user: #{@user} on channel #{@channel}."
        logger.debug message.inspect
        # Run the event in the context of the app
        @event_context.instance_exec message, &@events[event]
      rescue Oj::ParseError => e
        handle_error(parse_message(e))
      rescue StandardError => e
        handle_error(runtime(e))
      end

      def handle_error(
        type: :error,
        error_msg: { error: { name: :error, message: :error } }
      )
        logger.error ERRORS[type]
        logger.error e.message
        logger.error e.backtrace.join("\n")

        send_message(error_msg)
      end

      ##
      # Manage the WebSocket's connection being closed.
      #
      def on_shutdown
        logger.debug "Disconnecting user: #{@user} from channel: #{@channel}."
        @connections[@channel].delete(@user)
      end

      class << self
        # all external calls *MUST* pass through pub-sub machinary.
        # this allows websocket outbound messages to get distributed to other
        # servers/processes.

        def broadcast(channel, message, except = [])
          @pub_sub.broadcast(channel, message, except)
        end

        def send_message(channel, user, message)
          @pub_sub.send_message(channel, user, message)
        end

        ##
        # Broadcast a message to the whole channel.
        # Can be used to access it outside the router's scope, for instance, in a background process.
        #
        def broadcast_local(channel, message, except = [])
          logger.debug "Broadcasting message on channel: #{channel}. Message:"
          logger.debug message
          if @connections[channel].nil?
            logger.error "channel not configured: #{channel}"
            return nil
          end

          @connections[channel].each do |user, websocket|
            next if except.include?(user)

            write message, websocket
          end
        end

        ##
        # Send a message to a user on the channel
        # Can be used to access it outside the router's scope, for instance, in a background process.
        #
        def send_message_local(channel, user, message)
          logger.debug "Sending message: #{message} to user: #{user} on channel: #{channel}. Message"
          if @connections[channel].nil?
            logger.error "channel not configured: #{channel}"
            return nil
          end

          write message, @@connections[channel][user]
        end

        ##
        # Write a message to the WebSocket.
        #
        # It's a wrapper around the different websocket implementations.
        # This has to be implemented for each backend.
        def write(_message, _websocket)
          logger.error 'Override the write method on the WebSocket-specific backend.'
          raise NotImplementedError
        end

        # set active pubsub impl.  there is a 2-way handshake becaDriveruse the pub-sub
        # impl *must* be able to forward messages to local websocket connections
        def pub_sub=(pub_sub)
          @pub_sub = pub_sub
          @pub_sub.event_manager = self
        end
      end

      protected

      ##
      # Maintain the connection if ping frames are supported
      #
      def on_open(_event)
        logger.debug "Connection openned as user: #{@user} on channel: #{@channel}."
      end
    end
  end
end
