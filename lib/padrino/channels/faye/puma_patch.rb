# frozen_string_literal: true

module Faye
  ##
  # Patch to provide puma http server support
  #
  class RackStream
    def initialize(socket_object)
      @socket_object = socket_object
      @connection    = socket_object.env['em.connection']
      @stream_send   = socket_object.env['stream.send']
      @socket_object.env.delete_if { |key, _| key.match(/.*hijack.*/) }
      @connection.socket_stream = self if @connection.respond_to?(:socket_stream)
    end
  end
end
