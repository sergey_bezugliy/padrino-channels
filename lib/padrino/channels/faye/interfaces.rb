# frozen_string_literal: true

module Padrino
  module Channels
    module Faye
      ##
      # Faye interfaces module
      #
      module Interfaces
        def send_message(channel, user, message)
          Padrino::Channels::Faye::EventManager.send_message channel, user, message
        end

        def broadcast(channel, message, except = [])
          Padrino::Channels::Faye::EventManager.broadcast channel, message, except
        end
      end
    end
  end
end
