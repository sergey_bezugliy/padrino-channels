# frozen_string_literal: true

module Padrino
  module Channels
    module Faye
      ##
      # Faye routing adapter
      #
      module Routing
        require 'faye/websocket'

        ##
        # Creates a WebSocket endpoint using SpiderGazelle + libuv.
        #
        # It handles upgrading the HTTP connection for you.
        # You can nest this inside controllers as you would do with regular actions in Padrino.
        #
        def websocket(channel, *args, &block)
          get channel, *args do
            @driver = WebSocket::Driver.rack(request.env)
            # Let some other action try to handle the request if it's not a WebSocket.
            throw :pass unless ::Faye::WebSocket.websocket?(@driver)

            set_websocket_user
            conn = ::Faye::WebSocket.new(env, nil, { ping: 15 })
            Padrino::Channels::Faye::EventManager.new(params[:channel] || channel,
                                                      session['websocket_user'],
                                                      conn, self, &block)
            conn.rack_response
          end
        end
        alias conn websocket
      end
    end
  end
end
