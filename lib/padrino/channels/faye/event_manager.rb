# frozen_string_literal: true

module Padrino
  module Channels
    module Faye
      ##
      # Faye event manager module
      #
      class EventManager < BaseEventManager
        def initialize(channel, user, websocket, event_context, &block)
          websocket.on :open do |event|
            on_open event # &method(:on_open)
          end
          websocket.on :message do |event|
            on_message event.data, @websocket
          end
          websocket.on :close do |event|
            on_shutdown event # method(:on_shutdown)
          end

          super channel, user, websocket, event_context, &block
        end

        ##
        # Manage the WebSocket's connection being closed.
        #
        def on_shutdown(event)
          @pinger&.cancel
          super
        end

        ##
        # Write a message to the WebSocket.
        #
        def self.write(message, websocket)
          websocket.send ::Oj.dump(message)
        end

        protected

        ##
        # Maintain the connection if ping frames are supported
        #
        def on_open(event)
          super event

          @websocket.ping('pong')
        end
      end
    end
  end
end
