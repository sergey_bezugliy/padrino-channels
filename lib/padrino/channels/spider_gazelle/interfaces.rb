# frozen_string_literal: true

module Padrino
  module Channels
    module SpiderGazelle
      ##
      # SpiderGazelle interfaces module
      #
      module Interfaces
        def send_message(channel, user, message)
          Padrino::Channels::SpiderGazelle::EventManager.send_message channel, user, message
        end

        def broadcast(channel, message, except = [])
          Padrino::Channels::SpiderGazelle::EventManager.broadcast channel, message, except
        end
      end
    end
  end
end
