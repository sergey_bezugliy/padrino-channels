# frozen_string_literal: true

module Padrino
  module Channels
    module SpiderGazelle
      ##
      # SpiderGazelle routing adapter
      #
      module Routing
        require 'spider-gazelle/upgrades/websocket'

        ##
        # Creates a WebSocket endpoint using SpiderGazelle + libuv.
        #
        # It handles upgrading the HTTP connection for you.
        # You can nest this inside controllers as you would do with regular actions in Padrino.
        #
        def websocket(channel, *args)
          get channel, *args do
            # Let some other action try to handle the request if it's not a WebSocket.
            throw :pass unless request.env['rack.hijack']

            @event_context = self
            @channel = params[:channel] || channel

            # It's a WebSocket. Get the libuv promise and manage its events
            request.env['rack.hijack'].call.then do |_hijacked|
              hijack_channell
            end
          end
        end

        private

        def hijack_channell
          @conn = ::SpiderGazelle::Websocket.new hijacked.socket, hijacked.env

          set_websocket_user

          Padrino::WebSockets::SpiderGazelle::EventManager.new(
            @channel, session['websocket_user'], @conn, @event_context, &block
          )
          @conn.start
        end

        alias conn websocket
      end
    end
  end
end
