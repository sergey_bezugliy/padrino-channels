# frozen_string_literal: true

module Padrino
  module Channels
    module SpiderGazelle
      ##
      # SpiderGazelle event manager module
      #
      class EventManager < BaseEventManager
        def initialize(channel, user, websocket, event_context, &block)
          websocket.progress method(:on_message)
          websocket.finally method(:on_shutdown)
          websocket.on_open method(:on_open)

          super channel, user, websocket, event_context, &block
        end

        ##
        # Manage the WebSocket's connection being closed.
        #
        def on_shutdown
          @pinger&.cancel
          super
        end

        ##
        # Write a message to the WebSocket.
        #
        def self.write(message, websocket)
          websocket.text ::Oj.dump(message)
        end

        protected

        ##
        # Maintain the connection if ping frames are supported
        #
        def on_open(event)
          super event

          return unless @websocket.ping('pong')

          variation = rand(1..20_000)
          @pinger = @websocket.loop.scheduler.every 40_000 + variation, method(:do_ping)
        end

        ##
        # Ping the WebSocket connection
        #
        def do_ping(_time1, _time2)
          @websocket.ping 'pong'
        end
      end
    end
  end
end
