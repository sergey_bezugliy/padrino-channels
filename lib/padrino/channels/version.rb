# frozen_string_literal: true

module Padrino
  module Channels
    VERSION = '0.15.1'
  end
end
