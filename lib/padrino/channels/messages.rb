# frozen_string_literal: true

module Padrino
  module Channels
    ##
    # Message formatting, processing and logging module
    #
    module Message
      require 'padrino/channels/messages/descriptions'
      require 'padrino/channels/messages/error_handler'
    end
  end
end
