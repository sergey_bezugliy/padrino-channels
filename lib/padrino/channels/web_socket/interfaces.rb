# frozen_string_literal: true

module Padrino
  module Channels
    module WebSocket
      ##
      # WebsocketDriver interfaces module
      #
      module Interfaces
        def send_message(channel, user, message)
          Padrino::Channels::WebSocket::EventManager.send_message channel, user, message
        end

        def broadcast(channel, message, except = [])
          Padrino::Channels::WebSocket::EventManager.broadcast channel, message, except
        end
      end
    end
  end
end
