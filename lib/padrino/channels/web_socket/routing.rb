# frozen_string_literal: true

module Padrino
  module Channels
    module WebSocket
      ##
      # WebsocketDriver routing adapter
      #
      module Routing
        ##
        # Creates a WebSocket endpoint using SpiderGazelle + libuv.
        #
        # It handles upgrading the HTTP connection for you.
        # You can nest this inside controllers as you would do with regular actions in Padrino.
        #
        def websocket(channel, *args, &block)
          get channel, *args do
            # Let some other action try to handle the request if it's not a WebSocket.
            conn = Padrino::WebSockets::WebSocket::Connection.new(request.env)
            throw :pass unless conn.websocket?

            set_websocket_user

            Padrino::WebSockets::WebSocket::EventManager.new(params[:channel] || channel,
                                                             session['websocket_user'],
                                                             conn, self, &block)
            conn
          end
        end
        alias conn websocket
      end
    end
  end
end
