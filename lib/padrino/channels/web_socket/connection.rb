# frozen_string_literal: true

module Padrino
  module Channels
    module WebSocket
      ##
      # WebSocketDriver connection adapter
      #
      class Connection
        attr_reader :env, :url, :scheme, :driver

        def initialize(_env)
          @driver = ::WebSocket::Driver.server(self)

          @driver.on :connect, lambda { |_|
            @driver.start if websocket?
          }

          @driver.on :message, ->(e) { @driver.text(e.data) }
          @driver.on :close,   ->(_) { close_connection_after_writing }
        end

        def receive_data(data)
          @driver.parse(data)
        end

        def write(data)
          send_data(data)
        end

        def websocket?
          ::WebSocket::Driver.websocket?(@driver.env)
        end
      end
    end
  end
end
