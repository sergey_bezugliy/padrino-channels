# frozen_string_literal: true

module Padrino
  module Channels
    ##
    # Faye adapter
    #
    module Faye
      require 'padrino_websockets/faye/interfaces'
      require 'padrino_websockets/faye/routing'
      require 'padrino_websockets/faye/event_manager'
    end
  end
end
