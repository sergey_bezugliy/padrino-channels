# frozen_string_literal: true

module Padrino
  module Channels
    ##
    # SpiderGazelle adapter
    #
    module SpiderGazelle
      require 'padrino/channels/spider_gazelle/interfaces'
      require 'padrino/channels/spider_gazelle/routing'
      require 'padrino/channels/spider_gazelle/event_manager'
    end
  end
end
