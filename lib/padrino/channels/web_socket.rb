# frozen_string_literal: true

module Padrino
  module Channels
    ##
    # WebSocketDriver adapter
    #
    module WebSocket
      require 'websocket/driver'
      require 'eventmachine'
      require 'padrino/channels/web_socket/connection'
      require 'padrino/channels/web_socket/interfaces'
      require 'padrino/channels/web_socket/routing'
      require 'padrino/channels/web_socket/event_manager'
    end
  end
end
