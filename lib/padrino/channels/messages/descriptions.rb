# frozen_string_literal: true

module Padrino
  module Channels
    module Messages
      ##
      # Message static descriptions module
      #
      module Descriptions
        ##
        # Error descriptions
        #
        module Error
          MSG = {
            parse_message: "Can't parse the WebSocket's message",
            message_format: "Wrong message format. Expected: {event: 'ev', p1: 'p1', ...}",
            unsupported_event: "The event you requested isn't supported",
            runtime: 'Error while running the event handler',
            undefined: 'Undefined error'
          }.freeze
        end

        module Debug
          MSG = {
          }.freeze
        end
      end
    end
  end
end
