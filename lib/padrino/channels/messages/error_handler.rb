# frozen_string_literal: true

module Padrino
  module Channels
    module Messages
      ##
      # Error handler. Logs error and exception messages, formats corresponding response.
      #
      class ErrorHandler
        attr_accessor :logger_instance, :sender_instance, :debug_level, :logger_method, :sender_method

        attr_reader :type, :message, :exception, :details

        def initialize(
          debug_level = nil,
          logger_instance:,
          sender_instance: nil,
          logger_method: :error,
          sender_method: :send_message
        )
          @logger_method = logger_method
          @sender_method = sender_method
          @debug_level = (debug_level || ENV['DEBUG_LEVEL'] || 2).to_i
          @logger_instance = logger_instance
          @sender_instance = sender_instance
        end

        def msg(
          data: {
            type: nil,
            message: nil,
            exception: nil,
            details: ''
          },
          logger_method: @logger_method || :error,
          sender_method: @sender_method || :send_message
        )
          format(type: data[:type], message: data[:message], exception: data[:exception], details: data[:details])
          actions(logger_method, sender_method)
        end

        def format(type:, message: nil, exception: nil, details: nil)
          @type = type || :undefined
          @message = message || Descriptions::Error::MSG[type] || exception&.message
          @exception = exception
          format_details(details)
        end

        def format_details(details)
          if @message
            @details = details
          else
            @message = details&.empty? ? 'undefined' : details.to_s
          end
        end

        private

        def actions(logger_method, sender_method)
          log_error(logger_method) if @debug_level >= 1
          log_message(logger_method) if @debug_level >= 2
          log_backtrace(logger_method) if @debug_level >= 3
          send_message(sender_method, build_message) unless @sender_instance.nil?
        end

        def log_error(_logger_method)
          logging(@type == 'undefined' ? Descriptions::Error::MSG[:undefined] : @message)
        end

        def log_message(_logger_method)
          logging(@exception&.message)
        end

        def log_backtrace(_logger_method)
          logging(@exception&.backtrace&.join("\n") || '')
        end

        def logging(msg)
          @logger_instance.send(logger_method, msg) if msg
        end

        def build_message
          { type: @type }.merge error_message
        end

        def error_message
          {
            error: { name: @type, message: format_message }.merge(exception_message)
          }
        end

        def format_message
          [@message&.to_s, @details&.to_s].compact.join("\n")
        end

        def exception_message
          return {} unless @exception

          { exception: {
            message: (@exception&.message || ''),
            backtrace: (@exception&.backtrace&.join("\n") || '')
          } }
        end

        def send_message(sender_method, message)
          @sender_instance.send(sender_method, message)
        end
      end
    end
  end
end
