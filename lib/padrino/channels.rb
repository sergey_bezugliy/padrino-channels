# frozen_string_literal: true

module Padrino
  ##
  # WebSocket Pub/Sub Channel module
  #
  module Channels
    require 'padrino/channels/messages'
    require 'padrino/channels/base_event_manager'
    require 'padrino/channels/pub_sub'

    LOGGER = Padrino::Channels::Messages::ErrorHandler.new(
      logger_instance: Padrino::Logger.logger
    )

    ##
    # Helper module
    #
    module Helpers
      require 'securerandom'

      ##
      # Returns user of current session or newly generated uuid
      #
      def set_websocket_user
        session['websocket_user'] ||= SecureRandom.uuid
      end
    end

    class << self
      ##
      # Main class that register this extension.
      #
      def registered(app)
        app.set :websockets_pub_sub, Padrino::Channels::PubSub.new unless app.respond_to?(:websockets_pub_sub)
        switch_driver(app)
        app.helpers Padrino::Channels::Helpers
      end
      alias included registered

      private

      def switch_driver(app)
        if env_driver?(::SpiderGazelle)
          spider_gazelle(app)
        elsif env_driver?(::Faye)
          faye(app)
        elsif env_driver?(::WebSocket)
          web_socket_driver(app)
        else
          driver_undefined(app)
        end
      end

      def env_driver?(driver_class)
        defined?(driver_class) || (ENV['WEBSOCKET_DRIVER'] == driver_class.to_s)
      end

      def spider_gazelle(_app)
        require 'padrino/channels/spider_gazelle'
        load_modules(::SpiderGazelle)
      end

      def faye(_app)
        require 'padrino/channels/faye'
        ::Faye::WebSocket.load_adapter('thin') if defined?(::Thin)
        require 'padrino/channels/faye/puma_patch' if defined?(Puma)
        load_modules(::Faye)
      end

      def web_socket_driver(_app)
        require 'padrino/channels/websocket'
        load_modules(::WebSocket)
      end

      def driver_undefinedclassname
        logger.error %{Can't find a WebSockets backend. At the moment we only support
          SpiderGazelle and Faye Websockets friendly application backends (Puma and Thin work,
          Rainbows, Goliath and Phusion Passenger remain untested and may break).}
        raise NotImplementedError
      end

      def load_modules(driver_class)
        namespace = "Padrino::Channels::#{driver_class}"
        app.helpers Module.const_get("#{namespace}::Helpers")
        app.extend Module.const_get("#{namespace}::Routing")
        Module.const_get("#{namespace}::EventManager").pub_sub = app.websockets_pub_sub
      end
    end
  end
end
