# frozen_string_literal: true

##
# Root Padrino webframework module
#
module Padrino
  require 'padrino-core'
  require 'padrino/channels'
end
