# frozen_string_literal: true

##
# Stub overriding message loggers and senders
#
module Helpers
  class OutputStub
    attr_accessor :logger, :output, :custom_logger, :custom_output

    def initialize
      @output = []
      @logger = []
      @custom_logger = []
      @custom_output = []
    end

    def error(message)
      @logger << message
    end

    def custom_error(message)
      @custom_logger << message
    end

    def send_message(message)
      @output << message
    end

    def custom_send_message(message)
      @custom_output << message
    end
  end
end
