require 'rubygems'
require 'websocket-client-simple'

module Helpers
  class WSClient
    attr_accessor :ws, :stack, :thread

    def initialize(url = 'ws://localhost:8888')
      reset
      @thread = Thread.new do
        @ws = WebSocket::Client::Simple.connect url
        @ws.send STDIN.gets.strip
        Thread.current.exit if @exit_flag
      end
      @thread.join
    end

    def message(response='Status: message')
      @thread.thread_variable_get(:ws).on :message do |msg|
        add_to_stack(:message, :request, msg)
        @thread.thread_variable_get(:ws).send (response)
        add_to_stack(:message, :response, msg)
        @exit_flag = false
      end
    end

    def open(response='Status: open')
      @thread.thread_variable_get(:ws).on :open do
        add_to_stack(:open, :request)
        @thread.thread_variable_get(:ws).send response
        add_to_stack(:open, :response, response)
        @exit_flag = false
      end
    end

    def close(response='Status: closing')
      add_to_stack(:close, :request)
      @thread.thread_variable_get(:ws).on :close do
        @thread.thread_variable_get(:ws).send msg }
      end
      add_to_stack(:close, :response, response)
      set_exit_flag
    end

    def error(response='Status: error')
      @thread.thread_variable_get(:ws).on :error do |e|
        add_to_stack(:error, :request, e)
        @thread.thread_variable_get(:ws).send response
        add_to_stack(:close, :response, response)
        set_exit_flag
      end
    end

    private

    def set_exit_flag
      @thread.thread_variable_set(:exit_flag, true)
    end

    def reset
      @stack = []
      set_exit_flag
    end

    def add_to_stack(event, type, message="")
      @stack << { event: event, type: type, message: message  }
    end
  end
end
