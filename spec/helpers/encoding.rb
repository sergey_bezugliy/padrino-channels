# frozen_string_literal: true

module Helpers
  module Encoding
    def encode(message, encoding = nil)
      WebSocket::Driver.encode(message, encoding)
    end

    def bytes(string)
      string.bytes.to_a
    end
  end
end
