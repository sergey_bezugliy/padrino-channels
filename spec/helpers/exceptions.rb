# frozen_string_literal: true

module Helpers
  def self.format_message(msg_type, message, exception)
    {
      type: msg_type,
      message: message,
      exception: exception,
      expectations: {
        messages: [message, (exception&.message || ''), (exception&.backtrace&.join("\n") || '')]
      }
    }
  end

  def self.divide_by_zero
    1 / 0
  rescue StandardError => e
    format_message(:dividing_by_zero, 'Dividing by zero is not acceptable.', e)
  end

  def self.standard_error
    format_message(:standard_error, 'Standard Error', StandardError.new)
  end

  EXCEPTION_CASES = [
    divide_by_zero,
    standard_error
  ].freeze
end
