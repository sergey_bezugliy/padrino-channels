# frozen_string_literal: true

require 'spec_helper'
require_relative 'shared_examples/build_object_spec_example_group'
require_relative 'shared_examples/messages_by_type_spec_example_group'
require_relative 'shared_examples/messages_by_exception_spec_example_group'

RSpec.describe Padrino::Channels::Messages::ErrorHandler do
  subject { described_class }

  let(:descriptions) do
    Padrino::Channels::Messages::Descriptions::Error::MSG[type]
  end

  let(:debug_level) do
    ->(level) { ENV['DEBUG_LEVEL'] = level.to_s }
  end

  let(:regular_handler) do
    subject.new(
      logger_instance: Helpers::OutputStub.new,
      sender_instance: Helpers::OutputStub.new
    )
  end

  let(:custom_handler) do
    subject.new(
      logger_instance: Helpers::OutputStub.new,
      sender_instance: Helpers::OutputStub.new,
      logger_method: :custom_error,
      sender_method: :custom_send_message
    )
  end

  describe 'handler objects' do
    it_behaves_like 'build object'
  end

  describe 'builds message' do
    Padrino::Channels::Messages::Descriptions::Error::MSG.each_pair do |key, value|
      it_behaves_like 'builds message by the type', key, value
    end

    it_behaves_like 'builds message by the exception',
                    Helpers::EXCEPTION_CASES[0][:type],
                    Helpers::EXCEPTION_CASES[0][:message],
                    Helpers::EXCEPTION_CASES[0][:exception],
                    Helpers::EXCEPTION_CASES[0][:expectations]
    it_behaves_like 'builds message by the exception',
                    Helpers::EXCEPTION_CASES[1][:type],
                    Helpers::EXCEPTION_CASES[1][:message],
                    Helpers::EXCEPTION_CASES[1][:exception],
                    Helpers::EXCEPTION_CASES[1][:expectations]
  end
end
