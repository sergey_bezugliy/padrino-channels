# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'builds message by the type' do |msg_type, msg_description|
  let(:result) do
    { type: msg_type, error: { name: msg_type, message: msg_description } }
  end

  let(:error_instance) do
    debug_level.call 3
    regular_handler.msg(
      data: {
        type: msg_type
      }
    )
    regular_handler
  end

  it "for type: `#{msg_type}`" do
    expect(error_instance.message).to eq(msg_description)
    expect(error_instance.logger_instance.logger.first).to eq(msg_description)
    expect(error_instance.sender_instance.output.last).to eq(result)
  end
end
