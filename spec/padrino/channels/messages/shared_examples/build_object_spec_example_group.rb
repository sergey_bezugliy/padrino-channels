# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'build object' do
  it 'builds default logging and sender object' do
    expect(regular_handler).to be_instance_of(described_class)
    expect(regular_handler.logger_instance).to be_instance_of(Helpers::OutputStub)
    expect(regular_handler.sender_instance).to be_instance_of(Helpers::OutputStub)
    expect(regular_handler.logger_method).to be(:error)
    expect(regular_handler.sender_method).to be(:send_message)
  end

  it 'builds custom logging and sender object' do
    expect(custom_handler).to be_instance_of(described_class)
    expect(custom_handler.logger_method).to be(:custom_error)
    expect(custom_handler.sender_method).to be(:custom_send_message)
  end
end
