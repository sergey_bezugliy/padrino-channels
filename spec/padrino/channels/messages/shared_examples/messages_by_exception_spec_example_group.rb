# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'builds message by the exception' do |custom_type, custom_msg, exception, expectations|
  let(:exception) { exception }

  let(:error_instance) do
    debug_level.call 3
    regular_handler.msg(
      data: {
        type: custom_type,
        message: custom_msg,
        exception: exception
      }
    )
    regular_handler
  end

  let(:result_with_exception) do
    {
      type: custom_type,
      error: {
        name: custom_type,
        message: custom_msg,
        exception: {
          message: exception&.message || '',
          backtrace: exception&.backtrace&.join("\n") || ''
        }
      }
    }
  end

  it "for exception class: `#{exception.class}`" do
    # expect(error_instance.message).to eq(custom_msg)
    expect(error_instance.logger_instance.logger).to eq(expectations[:messages])
    expect(error_instance.sender_instance.output.last).to eq(result_with_exception)
  end
end
