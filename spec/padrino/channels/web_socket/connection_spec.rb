# frozen_string_literal: true

require 'spec_helper'
require 'websocket/driver'
require 'padrino/channels/web_socket/connection'

RSpec.describe Padrino::Channels::WebSocket::Connection do
  include Helpers::Encoding
  subject { described_class }

  let :body do
    encode [0x91, 0x25, 0x3e, 0xd3, 0xa9, 0xe7, 0x6a, 0x88]
  end

  let :response do
    string = "\xB4\x9Cn@S\x04\x04&\xE5\e\xBFl\xB7\x9F\x1D\xF9"
    string.force_encoding(Encoding::BINARY) if string.respond_to?(:force_encoding)
    string
  end

  let(:env) do
    {
      'REQUEST_METHOD' => 'GET',
      'HTTP_CONNECTION' => 'Upgrade',
      'HTTP_UPGRADE' => 'WebSocket',
      'HTTP_HOST' => 'localhost',
      'REQUEST_URI' => '/channels/main',
      'HTTP_ORIGIN' => 'http://localhost:3000',
      'HTTP_SEC_WEBSOCKET_KEY1' => '1   38 wZ3f9 23O0 3l 0r',
      'HTTP_SEC_WEBSOCKET_KEY2' => '27   0E 6 2  1665:< ;U 1H',
      'rack.input' => StringIO.new(body)
    }
  end

  let(:conn) do
    subject.new(env)
  end

  describe 'Websocket listener' do
    it { expect(conn).to be_kind_of(described_class) }

    describe 'Websocket driver instance:' do
      it('class of instance is correct') { expect(conn.driver).to be_kind_of(::WebSocket::Driver::Server) }
      it('provides :connect listener') { expect(conn.driver.listeners(:connect).first).to be_kind_of(Proc) }
      it('provides :message listener') { expect(conn.driver.listeners(:message).first).to be_kind_of(Proc) }
      it('provides :close listener') { expect(conn.driver.listeners(:close).first).to be_kind_of(Proc) }
    end
  end

  describe 'openes connection' do
    xit 'reads incoming raw binary data' do
    end
  end
end
