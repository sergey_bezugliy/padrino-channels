# Padrino Channels (new version of padrino-websockets)

A PubSub WebSockets Channels abstraction for the Padrino Ruby Web Framework to manage
channels, users and events regardless of the underlying WebSockets implementation.
## Coverage report

* [SimpleCov report](coverage/index.html) - the test coverage for current branch.

## Current support

* [websocket-driver](https://github.com/faye/websocket-driver-ruby) an implementation of WebSocket protocols that can be hooked up to any TCP library.  (**)
* [SpiderGazelle](https://github.com/cotag/spider-gazelle) a LibUV application server.
* [Puma](http://puma.io/) >= 2.0. (*)
* [Thin](http://code.macournoyer.com/thin/)  (*)
* [Rainbows](http://rainbows.rubyforge.org/)  (\*)  (**)
* [Goliath](http://postrank-labs.github.com/goliath/)  (\*) (**)
* [Phusion Passenger](https://www.phusionpassenger.com/) >= 4.0 with nginx >= 1.4  (\*)  (**)

(*) Supported through [faye-websocket-ruby](https://github.com/faye/faye-websocket-ruby).

(**) Test coverage. WIP.


## Installation

Add this line to your application's `Gemfile`:

```
gem 'padrino-channels'

# Uncomment the following line if you're using a Faye backend
# gem 'faye-websocket'
# Uncomment the following line if you're using a SpiderGazelle backend
# gem 'spider_gazelle'
# Uncomment the following line if you're using a WebSocketDriver backend
# gem 'websocket_driver'
```

And then execute:

```
$ bundle
```

Or install it yourself as:

```
$ gem install padrino-channels
```

## Usage

Add this line to your main application's file (`app/app.rb`):

```
register Padrino::Channels
```

Then in any controller or in the app itself, define a WebSocket channel:

```
websocket :channel do
  on :ping do |message|
    send_message(:ping, session['websocket_user'], {pong: true, data: message})
    broadcast(:ping, {pong: true, data: message, broadcast: true})
  end
end
```

How to use it on a browser?

```
var connection = new WebSocket('ws://localhost:3000/channel');
websockets
connection.onopen = function(message) {
  console.log('connected to channel');
  connection.send(JSON.stringify({event: 'ping', some: 'data'}));
}

connection.onmessage = function(message) {
  console.log('message', JSON.parse(message.data));
}

// TODO Implement on the backend
connection.onerror = function(message) {
  console.error('channel', JSON.parse(message.data));
}

```

## Contributing

1. Fork it ( http://github.com/<my-github-username>/padrino-channels/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request


## Contributors
Forked, updated, linted, covered with tests and continued as padrino-channels by @sbezugliy at [Codenv.top](https://codenv.top/about).

Made with <3 by @dariocravero at [UXtemple](http://uxtemple.com).

Heavily inspired by @stakach's [example](https://github.com/cotag/spider-gazelle/issues/4).
