# frozen_string_literal: true

##
# Padrino Pub/Sub channel application demo
#
module PadrinoWebsocketsDemo
  ##
  # Application class
  #
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::WebSockets

    enable :sessions

    get :index do
      render :index
    end

    websocket :channel do
      on :test do |message|
        # 'test on channel'
        send_message :channel, session['websocket_user'], message
        broadcast :channel, message.merge({ 'broadcast' => true })
      end
    end
  end
end
