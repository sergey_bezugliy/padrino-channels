# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'padrino/channels/version'

Gem::Specification.new do |spec|
  spec.required_ruby_version = '>= 2.5.2'
  spec.name          = 'padrino-channels'
  spec.version       = Padrino::Channels::VERSION
  spec.authors       = ['Darío Javier Cravero', 'Sergey Bezugliy']
  spec.email         = ['dario@uxtemple.com', 's.bezugliy@gmail.com']
  spec.summary       = 'A WebSockets abstraction for Padrino. Earlier padrino-websockets'
  spec.description   = 'A WebSockets abstraction for the Padrino Ruby Web Framework to manage
    channels, users and events regardless of the underlying WebSockets implementation.'
  spec.homepage      = 'https://github.com/dariocravero/padrino-websockets'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(spec)/})
  spec.require_paths = ['lib']

  # TODO: add switching mechanism between websocket drivers
  spec.add_dependency 'oj'
  spec.add_dependency 'padrino-core', '0.15.1'
  spec.add_dependency 'rake'
  spec.add_development_dependency 'bundler', '~> 2.1.4'
  spec.add_development_dependency 'eventmachine'
  spec.add_development_dependency 'faye'
  spec.add_development_dependency 'guard'
  spec.add_development_dependency 'guard-rspec'
  spec.add_development_dependency 'guard-rubocop'
  spec.add_development_dependency 'libuv'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'rubocop-rake'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'spider-gazelle'
  spec.add_development_dependency 'websocket-client-simple'
  spec.add_development_dependency 'websocket-driver'
end
